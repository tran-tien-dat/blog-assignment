**Name:** Tran Tien Dat

**Matric. No.:** A0131140E

This is a simple blog created for  CVWO's Assignment 1. It is live at http://blogtest-trantiendat.rhcloud.com/ you can log in with as a test user with testuser as username and 123456 as password

####TODOs:
* Improve security
* Improve error handling & feedback
* Improve organisation (probably using AJAX)
* Improve UI
