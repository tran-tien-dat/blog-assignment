<?php require_once('config/config.php');
require_once('model/database.php');
require_once('lib/Parsedown/Parsedown.php');
require_once('lib/password_compat/password.php');
require_once('model/utils.php');

if (empty($_GET['action'])) {
    $numPage = get_num_page();
    $curPage = empty($_GET['page']) ? 1 : (int) $_GET['page'];

    if ($curPage > 0 && $curPage <= $numPage) {
        // valid page number, display the page
        require_once('view/home.php');
    } else {
        header('Location: /');
    }
} elseif (is_logged_in() && $_GET['action'] === 'create') {
    $isCreating = true;
    $isSaved = isset($_POST['id']);
    if ($isSaved) {
        // query database and define $row
        $row = get_saved_post_by_id($_POST['id']);
        if (!$row || $row['author_id'] !== $_SESSION['userID']) {
            // invalid saved post id or insufficient privilege,
            // treat as creating new post
            $isSaved = false;
        }
    }
    require_once('view/post_editor.php');
} elseif (is_logged_in() && $_GET['action'] === 'view_saved') {
    require_once('view/saved_list.php');
}else {
    header('Location: /');
}
?>
