<?php
require_once('../config/config.php');
require_once('database.php');
require_once('../lib/Parsedown/Parsedown.php');
require_once('../lib/password_compat/password.php');
require_once('utils.php');

if (!empty($_POST['action'])) {
    switch ($_POST['action']) {
        case 'login':
            $result = array('successful_login' => false);
            if (!is_logged_in() && 
                !empty($_POST['username']) && !empty($_POST['password'])) {
                // validate input
                $username = check_input($_POST['username']);
                $password = check_input($_POST['password']);
                
                if ($row = log_in($username, $password)) {
                    // Save login details
                    $_SESSION['userID'] = $row['id'];
                    $_SESSION['username'] = $row['username'];
                    $_SESSION['userDisplayName'] = $row['name'];
                    $result = array('successful_login' => true,
                                    'user_display_name' => $row['name']);
                }
            }
            break;
        case 'logout':
            $result = array('successful_logout' => false);
            session_unset();
            session_destroy();
            $result = array('successful_logout' => true);
            break;
        case 'delete':
            $result = array('successful_delete' => false);
            if (is_logged_in() && isset($_POST['postID'])) {
                $postID = check_input($_POST['postID']);
                if (delete_post($postID)) {
                    $result = array('successful_delete' => true);
                }
            }
            break;
        case 'publish':
            $result = array('successful_publish' => false);
            if (is_logged_in() && isset($_POST['postID'])
                && !empty($_POST['body']) && !empty($_POST['title'])) {
                // Validate input
                // does not validate body though
                // special html characters in body will be escaped
                // in the markdowwn parser
                // $body = check_input($_POST['body']);
                $title = check_input($_POST['title']);

                $body = $_POST['body'];
                $authorID = $_SESSION['userID'];

                // $saved_postID !== '0' means need to delete the saved post
                $saved_postID = $_POST['postID'];
                
                // Publish the post, if $saved_postID !== '0', check whether
                // the user is the author of the saved post, if also delete
                // the post from the saved post list
                if ($postID = publish_post($body, $title, $authorID)) {
                    $result = array('successful_publish' => true,
                        'postID' => $postID);
                    if (($saved_postID !== '0' )
                        && ($post = get_saved_post_by_id($saved_postID))
                        && ($post['author_id'] === $authorID)) {
                        delete_saved_post($saved_postID);
                     }
                }
            }
            break;
        case 'edit':
            $result = array('successful_edit' => false);
            if (is_logged_in() && !empty($_POST['postID'])
                && !empty($_POST['body']) && !empty($_POST['title'])) {
                // Validate input
                // does not validate body though
                // special html characters in body will be escaped
                // in the markdowwn parser
                // $body = check_input($_POST['body']);
                $title = check_input($_POST['title']);

                $body = $_POST['body'];

                $editorID = $_SESSION['userID'];
                $postID =  $_POST['postID'];

                if (edit_post($postID, $body, $title, $editorID)) {
                    $result = array('successful_edit' => true);
                }
            }
            break;
        case 'preview':
            $result = array('marked_up' => '');
            if (is_logged_in() && !empty($_POST['body'])) {
                $result['marked_up'] = render_body($_POST['body']);
            }
            break;
        case 'save':
            $result = array('successful_save' => false);
            if (is_logged_in() && isset($_POST['postID'])
                && !empty($_POST['body']) && !empty($_POST['title'])) {
                // Validate input
                // does not validate body though
                // special html characters in body will be escaped
                // in the markdowwn parser
                // $body = check_input($_POST['body']);
                $title = check_input($_POST['title']);

                $body = $_POST['body'];
                $postID = $_POST['postID'];
                $authorID = $_SESSION['userID'];

                if ($postID === '0') {
                    // Save a new post
                    if ($postID = save_new_post($body, $title, $authorID)) {
                        $result = array('successful_save' => true,
                                        'postID' => $postID);
                    }
                } else {
                    // Edit an already saved post
                    // Check whether user is post's author
                    $post = get_saved_post_by_id($postID);
                    if ($post['author_id'] === $authorID
                        && edit_saved_post($postID, $body, $title, $authorID)) {
                        $result = array('successful_save' => true,
                            'postID' => $postID);
                    }
                }
            }
            break;
        case 'delete_saved':
            $result = array('successful_delete' => false);
            if (is_logged_in() && isset($_POST['postID'])) {
                $postID = check_input($_POST['postID']);
                $userID = $_SESSION['userID'];
                if (($post = get_saved_post_by_id($postID))
                    && ($post['author_id'] === $userID)
                    && delete_saved_post($postID)) {
                    $result = array('successful_delete' => true);
                }
            }
            break;
        default:
            $result = array('message' => 'Invalid request');
    }
    echo json_encode($result);
}
?>
