<?php
function connect_to_db() {
    // Remember to set $conn=null after done
    try {
        $conn = new PDO("mysql:host=".DBHOST.";port=".DBPORT
                        .";dbname=".DBNAME, DBUSER, DBPASS);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
        return false;
    }
}

function get_post_by_id($postID) {
    $result = false;
    $conn = connect_to_db();
    if ($conn) {
        try {
            $stmt = $conn->prepare('SELECT blogpost.id, title, body,
                                    published_time, last_edited_time,
                                    author_user.name AS author,
                                    editor_user.name AS editor
                                    FROM blogpost
                                      INNER JOIN user as author_user
                                        ON blogpost.author_id
                                            = author_user.id
                                      LEFT JOIN user as editor_user
                                        ON blogpost.last_editor_id
                                            = editor_user.id
                                    WHERE blogpost.id = :postID;');
            $stmt->execute(array(':postID' => $postID));
            $row = $stmt->fetch();
            $result = $row;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    } 
    return $result;
}

function get_posts_by_page($page_num) {
    $result = false;
    if ($conn = connect_to_db()) {
        try {
            $limit = NUM_POST_PER_PAGE;
            $offset = NUM_POST_PER_PAGE * ($page_num - 1);
            $stmt = $conn->query('SELECT blogpost.id, title, body,
                                  published_time, last_edited_time,
                                  author_user.name AS author,
                                  editor_user.name as editor
                                  FROM blogpost
                                    INNER JOIN user as author_user
                                      ON blogpost.author_id 
                                        = author_user.id
                                    LEFT JOIN user as editor_user
                                      ON blogpost.last_editor_id
                                        = editor_user.id
                                    ORDER BY blogpost.id DESC
                                    LIMIT '.$limit.' OFFSET '.$offset.';');
            $result = $stmt->fetchAll();
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }
    return $result;
}

function get_num_page() {
    $result = false;
    if ($conn = connect_to_db()) {
        try {
            $stmt = $conn->query("SELECT COUNT(*) FROM blogpost;");
            $row = $stmt->fetch();
            $result = ceil($row[0] / NUM_POST_PER_PAGE);
            $result = (int) $result;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }
    return $result;
}

function get_all_saved_post($author_id) {
    $result = false;
    if ($conn = connect_to_db()) {
        try {
            $stmt = $conn->prepare('SELECT saved_post.id, title, body
                                  FROM saved_post
                                    INNER JOIN user
                                      ON saved_post.author_id = user.id
                                  WHERE author_id = :authorID
                                    ORDER BY saved_post.id DESC;');
            $stmt->execute(array('authorID' => $author_id));
            $result = $stmt->fetchAll();
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }
    return $result;
}

function get_saved_post_by_id($postID) {
    // Note: saved unpublished post has a different id from published post
    $result = false;
    $conn = connect_to_db();
    if ($conn) {
        try {
            $stmt = $conn->prepare('SELECT id, title, body, author_id
                                    FROM saved_post
                                    WHERE id = :postID;');
            $stmt->execute(array(':postID' => $postID));
            $row = $stmt->fetch();
            $result = $row;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    } 
    return $result;
}

function log_in($username, $password) {
    $result = false;
    if ($conn = connect_to_db()) {
        try {
            $stmt = $conn->prepare('SELECT * FROM user
                                    WHERE username=:username;');
            $stmt->execute(array(':username' => $username));
            $row = $stmt->fetch();
            if (password_verify($password, $row['password'])) {
                $result = $row;
            }
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }
    return $result;
}

function delete_post($postID) {
    $result = false;
    if ($conn = connect_to_db()) {
        try {
            $stmt = $conn->prepare('DELETE FROM blogpost WHERE id= :postID');
            $stmt->execute(array(':postID' => $postID));
            $result = true;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }
    return $result;
}

function publish_post($body, $title, $authorID) {
    $result = false;
    if ($conn = connect_to_db()) {
        try {
            $stmt = $conn->prepare('INSERT INTO blogpost (title, body,
                                    author_id, published_time)
                                    VALUES (:title, :body, :author_id,
                                    :time)');
            $stmt->execute(array(':title' => $title,
                                 ':body' => $body,
                                 ':author_id' => $authorID,
                                 ':time' => date('Y-m-d H:i:s')));
            if ($postID = $conn->lastInsertId()) {
                $result = $postID;
            }
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }
    return $result;
}

function edit_post($postID, $body, $title, $editorID) {
    $result = false;
    if ($conn = connect_to_db()) {
        try {
            $stmt = $conn->prepare('UPDATE blogpost
                                    SET title=:title, body=:body,
                                        last_edited_time=:time,
                                        last_editor_id=:editorID
                                    WHERE id=:postID');
            $stmt->execute(array(':title' => $title,
                                 ':body' => $body,
                                 ':editorID' => $editorID,
                                 ':time' => date('Y-m-d H:i:s'),
                                 ':postID' => $postID));
            $result = true;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }
    return $result;
}

function save_new_post($body, $title, $authorID) {
    $result = false;
    if ($conn = connect_to_db()) {
        try {
            $stmt = $conn->prepare('INSERT INTO saved_post (title, body,
                                    author_id)
                                    VALUES (:title, :body, :author_id)');
            $stmt->execute(array(':title' => $title,
                                 ':body' => $body,
                                 ':author_id' => $authorID));
            if ($postID = $conn->lastInsertId()) {
                $result = $postID;
            }
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }
    return $result;
}

function edit_saved_post($postID, $body, $title, $authorID) {
    $result = false;
    if ($conn = connect_to_db()) {
        try {
            $stmt = $conn->prepare('UPDATE saved_post
                                    SET title=:title, body=:body,
                                        author_id=:authorID
                                    WHERE id=:postID');
            $stmt->execute(array(':title' => $title,
                                 ':body' => $body,
                                 ':authorID' => $authorID,
                                 ':postID' => $postID));
            $result = true;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }
    return $result;
}

function delete_saved_post($postID) {
    $result = false;
    if ($conn = connect_to_db()) {
        try {
            $stmt = $conn->prepare('DELETE FROM saved_post WHERE id= :postID');
            $stmt->execute(array(':postID' => $postID));
            $result = true;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }
    return $result;
}
?>
