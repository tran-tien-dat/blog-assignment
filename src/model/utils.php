<?php
function is_logged_in() {
    return isset($_SESSION['userID'], $_SESSION['username'],
                 $_SESSION['userDisplayName']);
}

function check_input($input) {
    // TODO: understand what this is for
    return htmlspecialchars(stripslashes($input));
}

function render_body($text) {
    $renderer = new Parsedown();
    // Escape special characters
    $renderer->setMarkupEscaped(true);
    return $renderer->text($text);
}

function truncate_body($text) {
    //$result = (strlen($text) > NUM_CHAR_PER_EXCERPT)
    //    ? mb_substr($text, 0, NUM_CHAR_PER_EXCERPT, 'utf-8').'...'
    //    : $text;

    // Remove the trailing ...
    $result = mb_substr($text, 0, NUM_CHAR_PER_EXCERPT, 'utf-8');
    // this is not perfect and may cut the post before closing the formatting
    return render_body($result);
}
?>
