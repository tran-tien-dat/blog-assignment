<?php require_once('config/config.php');
require_once('model/database.php');
require_once('lib/Parsedown/Parsedown.php');
require_once('lib/password_compat/password.php');
require_once('model/utils.php');

$row = get_post_by_id($_GET['id']);
if ($row) {
    require_once('view/single_post.php');
} else {
    // redirect to homepage if post does not exist
    header('Location: /');
}
?>


