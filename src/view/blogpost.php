<?php
function display_blogpost($row, $full_post) {
    // Display the content of a post as returned from the database in $row
    // $full_post (boolean) indicate whether to display the entire post
    // or truncate
?>
    <div class="blog-post" id="post-<?php echo $row['id']?>">
        <div class="writer-btns pull-right"<?php echo is_logged_in()? '': ' hidden';?>>
            <div class="btn-group">
                <button class="btn btn-warning" type="button" onclick="edit_post(<?php echo $row['id'] ?>)"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</button>
                <button class="btn btn-danger" type="button" onclick="delete_post(<?php echo $row['id'].', '.($full_post ? 'true' : 'false') ?>)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</button>
            </div>
        </div>
        <h1 class="blog-title"><?php 
            echo $full_post ? $row['title']
                    : '<a href="viewpost.php?id='.$row['id'].'">'.$row['title'].'</a>' ?></h1>
        <p class="blog-author-text"><span class="glyphicon glyphicon-star blog-star" aria-hidden="true"></span> By <?php echo $row['author'] ?></p>
        <p class="blog-datetime-text">Posted on <?php 
        $published_time = strtotime($row['published_time']);
        echo date('d M Y', $published_time).' at '
            .date('h:i a', $published_time);
        if (!is_null($row['editor'])
            && !is_null($row['last_edited_time'])) {
            $last_edited_time = strtotime($row['last_edited_time']);
            echo '<br>Last edited by '.$row['editor'].' on '
                .date('d M Y', $last_edited_time).' at '
                .date('h:i a', $last_edited_time);
        } 
        echo '</p>';
        
    if ($full_post) {
        echo '<div class="blog-body">'
            .render_body($row['body']).'</div>';
    } else {
        echo '<div class="blog-post-excerpt">'
            .truncate_body($row['body']).'</div>';
        echo '<p>...........</p>
            <p><a href="viewpost.php?id='.$row['id'].
            '">Read more</a></p>';
    } ?>
        <hr>
    </div>
<?php } ?>
