<nav class="navbar navbar-static-top blog-navbar">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                data-toggle="collapse" data-target="#navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Tien Dat's Blog</a>
        </div>

        <div class="collapse navbar-collapse" id="navbar">
            <div id="writer-nav"<?php echo is_logged_in() ? '' : ' hidden';?>>
                <ul class="nav navbar-nav blog-nav">
                    <li><a href="index.php?action=create"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create</a></li>
                    <li><a href="index.php?action=view_saved"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> My Saved Posts</a></li>
                </ul>
                <div class="navbar-right">
                    <p id="welcome-msg" class="navbar-text">Welcome <?php echo is_logged_in() ? $_SESSION['userDisplayName'] : ''?></p>
                    <button id="logout-btn" class="btn navbar-btn btn-primary"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Log out</button>
                </div>
            </div>

            <div id="reader-nav"<?php echo is_logged_in() ? 'hidden' : '';?>>
                <form id="login-form" class="navbar-form navbar-right" role="form" method="post">
                    <div class="form-group">
                        <input class="form-control" id="username" type="text"
                            name="username" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <input class="form-control" id="pwd" type="password"
                             name="password" placeholder="Password">
                    </div>
                    <button id="login-btn" type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Log in</button>
                </form>
            </div>
        </div>
    </div>
</nav>

<header class="blog-header">
    <div class="header">
        <h1>Tien Dat's Blog</h1>
        <p>Random stuff awaits</p>
        <p><span class="glyphicon glyphicon-star blog-star" aria-hidden="true"></span><p>
    </div>
</header>

