<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Tien Dat's Blog - <?php echo !$isCreating ? 'Edit post' : 'Create a new post'?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="static/css/blog.css">                      
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script>
        // global variables to tell javascript
        var is_creating = <?php echo $isCreating ? 'true' : 'false' ?>;
        var postID = <?php echo (!$isCreating || $isSaved)
                                ? $row['id'] : '0'?>;
    </script>
    <script src="static/js/post_editor.js"></script>
    <script src="static/js/log_in_out.js"></script>
</head>

<body>
    <?php include 'view/common/header.php'?>
    <div class="container blog-content"> <div class="row">
        <form class="col-md-6" role="form" method="post">
            <div class="form-group">
                <label for="title">Title:</label>
                <input class="form-control" id="title"
                    type="text" name="title"
                    value="<?php 
                        echo (!$isCreating || $isSaved) 
                                ? $row['title'] : ''
                    ?>">
                <br>
            </div>
            <div class="form-group">
                <label for="body">Body:</label>
                <textarea class="form-control" id="body"
                    name="body" rows="15"><?php
                    echo (!$isCreating || $isSaved) 
                            ? $row['body'] : ''
                ?></textarea>
            </div>

            <div class="btn-group">
            <input id="submit-btn" class="btn btn-success" type="submit"
                value="<?php echo !$isCreating ? 'Update' : 'Publish'?>">
            <input id="save-btn" class="btn btn-default<?php 
                echo !$isCreating ? ' hidden' : ''?>"
                type="button" value="Save">
            <input id="preview-btn" class="btn btn-default"
                type="button" value="Preview">
            <input id="cancel-btn" class="btn btn-default"
                type="button" value="Cancel">
            </div>
            <div id="submit-feedback" class="alert alert-danger"
                role="alert" hidden></div>
        </form>
        <div class="col-md-6">
            <h4>Preview:</h4>
            <div id="preview-wrapper" hidden>
                <h1 id="preview-title" class="blog-title"></h1>
                <div id="preview-body" class="blog-body"></div>
            </div>
        </div>
    </div> </div>
    <?php include 'view/common/footer.php'?>
</body>
</html>
