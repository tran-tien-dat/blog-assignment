<?php require_once('view/blogpost.php');?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Tien Dat's Blog</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="static/css/blog.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="static/js/blogpost.js"></script>
    <script src="static/js/log_in_out.js"></script>
</head>

<body>
    <?php
    include 'view/common/header.php';
    ?>
    <div class="container blog-content">
        <div class="row">
            <!--blank column to center main column-->
            <div class="col-md-2"></div>
            <div class="col-md-8">
            <?php
            if ($posts = get_posts_by_page($curPage)) {
                foreach ($posts as $row) {
                    display_blogpost($row, false); 
                }
            } ?>
            </div>
        </div>
    </div>
    <?php 
        include 'view/pagination.php';
        include 'view/common/footer.php';
    ?>
</body>
</html>
