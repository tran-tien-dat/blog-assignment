<?php require_once('view/blogpost.php');?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Tien Dat's Blog - My Saved Posts</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="static/css/blog.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="static/js/blogpost.js"></script>
    <script src="static/js/log_in_out.js"></script>
</head>

<body>
    <?php
    include 'view/common/header.php';
    ?>
    <div class="container blog-content"><div class="row">
        <!--Hidden column to center main column-->
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <?php
            if ($posts = get_all_saved_post($_SESSION['userID'])) {
                echo '<h1>My Saved Posts</h1>';
                foreach ($posts as $row) {?>
            <div class="blog-saved-list" id="post-<?php echo $row['id']?>">
                <span><?php echo $row['title']?></span>
                <div class="btn-group">
                    <button class="btn btn-warning btn-sm" type="button" onclick="edit_post(<?php echo $row['id'] ?>, true)"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</button>
                    <button class="btn btn-danger btn-sm" type="button" onclick="delete_post(<?php echo $row['id'] ?>, false, true)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</button>
                </div>
            </div>
                <?php }
            } else {
                echo '<div class="alert alert-info" role="alert"> You do not have any saved post.</div>';
            } ?>
        </div>
    </div></div>

    <!--Hidden form to send saved post's id-->
    <form id="postID-form" class="hidden" method="POST" action="index.php?action=create">
        <input id="postID" type="hidden" name="id">
    </form>
    <?php 
        include 'view/common/footer.php';
    ?>
</body>
</html>

