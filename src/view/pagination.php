    <div class="container">
        <nav class = "text-center"> <ul class="pagination">
        <?php
            $pathStr = 'index.php?page=';
            $startPage = ($curPage + 2 > $numPage)
                            ? $numPage - 4 : $curPage - 2;
            $startPage = $startPage < 1 ? 1 : $startPage;
            $endPage = ($startPage + 4 > $numPage) ? $numPage : $startPage + 4;

            // Previous button
            if ($curPage === 1) {   // disable previous button
                echo '<li class="disabled"><span><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></span></li>';
            } else {
                echo '<li><a href="'.$pathStr.($curPage - 1).'"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>';
            }

            // Numbered buttons
            for ($page = $startPage; $page <= $endPage; $page++) {
                echo '<li'.($page === $curPage ? ' class="active"' : '')
                    .'><a href="'.$pathStr.$page.'">'.$page.'</a></li>';
            }
            
            // Next button
            if ($curPage === $numPage) {   // disable next button
                echo '<li class="disabled"><span><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></span></li>';
            } else {
                echo '<li><a href="'.$pathStr.($curPage + 1).'"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>';
            }
        ?>
        </ul> </nav>
    </div>
