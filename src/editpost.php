<?php require_once('config/config.php');
require_once('model/database.php');
require_once('lib/Parsedown/Parsedown.php');
require_once('lib/password_compat/password.php');
require_once('model/utils.php');
if (!is_logged_in()
    || empty($_GET['id'])) {
    // Invalid request. Return to homepage
    header('Location: /');
}

$row = get_post_by_id($_GET['id']);
if ($row) {
    $isCreating = false;
    require_once('view/post_editor.php');
} else {
    // redirect to homepage if post does not exist
    header('Location: /');
}
?>
