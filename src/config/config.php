<?php
session_start();
// database credentials
define("DBHOST", "localhost");
define("DBPORT", "3306");
define("DBUSER", "blogapp");
define("DBPASS", "password");
define("DBNAME", "blog_assignment");

define("NUM_POST_PER_PAGE", 8);
define("NUM_CHAR_PER_EXCERPT", 1000);

// set timezone
date_default_timezone_set('Asia/Singapore');
?>
