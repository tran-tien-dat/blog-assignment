function delete_post(postID, is_single_post, is_saved_post) {
    is_saved_post = (is_saved_post === undefined) ? false : true;

    if (confirm("Are you sure to delete this post?")) {
        $.ajax({
            url: "model/user.php",
            data: {
                action: is_saved_post ? "delete_saved" : "delete",
                postID: postID
            },
            type: "POST",
            dataType: "json",
            success: function(json) {
                // TODO: handle deletion error
                if (json.successful_delete) {
                    alert("Post deleted successfully");
                    if (is_single_post) {
                        // Redirect to homepage if in single post view
                        window.location.assign("/");
                    } else {
                        // if not, hide the post and remove it
                        $("#post-" + postID).
                            hide("fast",
                                  function() {
                                      $("#post-" + postID).remove();
                                  }); 
                    }
                }
            }
        });
    }
}

function edit_post(postID, is_saved_post) {
    is_saved_post = (is_saved_post === undefined) ? false : true;

    if (is_saved_post) {
        $("#postID").val(postID);
        $("#postID-form").submit();
    } else {
        window.location.assign("editpost.php?id=" + postID);
    }
}
