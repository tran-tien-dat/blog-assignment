$(document).ready(function() {
    $("#login-form").submit(function(event) {
        // Prevent default submit action. Submit action will be handled here
        event.preventDefault();

        // Enforce required field
        if ($("#username").val() === "" || $("#pwd").val() === "") {
            alert("Please fill in BOTH username AND password");
        } else {
            // Disable login button to indicate logging in
            $("#login-btn").prop("disabled", true);
            $("#login-btn").text("Logging in...");
            // Sending AJAX request
            $.ajax({
                url: "model/user.php",
                data: {
                    action: "login",
                    username: $("#username").val(),
                    password: $("#pwd").val()
                },
                type: "POST",
                dataType: "json",
                success: function(json) {
                    // Enable login button
                    $("#login-btn").prop("disabled", false);
                    $("#login-btn").text("Log in");

                    if (json.successful_login) {
                        $("#welcome-msg").text("Welcome "
                                                + json.user_display_name);
                        $("#reader-nav").fadeOut("fast", function() {
                            $("#writer-nav").fadeIn("fast");
                            $(".writer-btns").fadeIn("fast");
                        });
                    } else {
                       alert("Invalid username and/or password");
                    }
                }
            });
        }
    });

    $("#logout-btn").click(function(event) {
        event.preventDefault();
        // Disable login button to indicate logging in
        $("#logout-btn").prop("disabled", true);
        $("#logout-btn").text("Logging out...");
        // Sending AJAX request
        $.ajax({
            url: "model/user.php",
            data: {
                action: "logout"
            },
            type: "POST",
            dataType: "json",
            success: function(json) {
                // TODO: handle loging out error
                if (json.successful_logout) {
                    alert("Logged out successfully. Redirecting to homepage");
                    window.location.assign("/");
                }
            }
        });
    });
});

