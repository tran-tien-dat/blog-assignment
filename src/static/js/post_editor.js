$(document).ready(function() {
    function validate_input(event) {
        // Make sure that user enters title and body
        event.preventDefault();

        $("#submit-feedback").hide("fast");

        if ($("#title").val() === "" || $("#body").val() === "") {
            // Notify that title or body is missing
            $("#submit-feedback").text("Please enter BOTH title and body!");
            $("#submit-feedback").show("fast");
            return false;
        } else {
            return true;
        }
    }

    $("#body").keydown(function(event) {
        if(event.keyCode === 9) { // tab was pressed
            // get caret position/selection
            var start = this.selectionStart;
            var end = this.selectionEnd;

            var $this = $(this);
            var value = $this.val();

            // set textarea value to: text before caret + tab + text after caret
            $this.val(value.substring(0, start)
                        + "\t"
                        + value.substring(end));

            // put caret at right position again (add one for the tab)
            this.selectionStart = this.selectionEnd = start + 1;

            // prevent the focus lose
            event.preventDefault();
        }
    });
        
    $("#cancel-btn").click(function() {
        if (confirm(
                "Are you sure to discard all the changes you have made?")) {
            window.location.assign("/");
        }
    });

    $("#submit-btn").click(function(event) {
        if (validate_input(event)) {
            if (is_creating) {
                // Disable the publish button
                $(this).prop("disabled", true);
                $(this).val("Publishing...");
                // AJAX query
                $.ajax({
                    url: "model/user.php",
                    data: {
                        action: "publish",
                        title: $("#title").val(),
                        body: $("#body").val(),
                        postID: postID
                    },
                    type: "POST",
                    dataType: "json",
                    success: function(json) {
                        // Enable publish button
                        $("#submit-btn").prop("disabled", false);
                        $("#submit-btn").text("Publish");

                        if (json.successful_publish) {
                            alert("Your post has been published successfully!");
                            // Redirect to new post
                            window.location.assign("viewpost.php?id="
                                                    + json.postID);
                        }
                    }
                });
            } else {
                // Disable the update button
                $(this).prop("disabled", true);
                $(this).val("Updating...");
                // AJAX query
                $.ajax({
                    url: "model/user.php",
                    data: {
                        action: "edit",
                        title: $("#title").val(),
                        body: $("#body").val(),
                        postID: postID
                    },
                    type: "POST",
                    dataType: "json",
                    success: function(json) {
                        // Enable publish button
                        $("#submit-btn").prop("disabled", false);
                        $("#submit-btn").text("Update");

                        if (json.successful_edit) {
                            alert("Your post has been edited successfully!");
                            // Redirect to updated post
                            window.location.assign("viewpost.php?id="
                                                    + postID);
                        }
                    }
                });
            }
        }       
    });

    $("#preview-btn").click(function(event) {
        if (validate_input(event)) {
            $("#preview-btn").val("Updating Preview...");

            $.ajax({
                url: "model/user.php",
                data: {
                    action: "preview",
                    body:  $("#body").val()
                },
                type: "POST",
                dataType: "json",
                success: function(json) {
                    $("#preview-btn").val("Preview");
                    $("#preview-wrapper").fadeOut("fast", function() {
                        //Update preview content
                        $("#preview-title").html($("#title").val());
                        $("#preview-body").html(json.marked_up);
                        
                        $("#preview-wrapper").fadeIn();
                    });
                }
            });
        }
    });

    $("#save-btn").click(function(event) {
        // Ensure valid input & working with an unpublished post
        if (validate_input(event) && is_creating) {
            $("#save-btn").val("Saving...");

            var title = $("#title").val();
            var body = $("#body").val();

            $.ajax({
                url: "model/user.php",
                data: {
                    action: "save",
                    title: title,
                    body: body,
                    postID: postID, // postID === 0 for completely new post
                },
                type: "POST",
                dataType: "json",
                success: function(json) {
                    $("#save-btn").val("Save");
                    if (json.successful_save) {
                        postID = json.postID;
                        alert("Your post has been saved successfully!");
                    }
                }
            });
        }
    });
});
